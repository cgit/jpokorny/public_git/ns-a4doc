#!/usr/bin/env python
# vim: set fileencoding=UTF-8:
# Copyright 2013 Red Hat, Inc.
# Author: Jan Pokorný <jpokorny at redhat dot com>
"""Documentation vocabulary for semantic highlights of eligible properties

This vocabulary serves a purpose of annotating specific notable semantic
properties accompanied with (optional) textual details or just standalone
hints connected with annotated entity, both of which are typically reflected
in some form of documentation (hence a4doc as typical prefix).

Primarily created for custom RELAX NG schema annotations via elements and
attributes in the external namespace (see the example).
"""
__author__ = u"Jan Pokorný"


from sys import argv
#from genshi.input import XML

from ontogen.ontogen import Property, Class, Example, OntologyWithFigure, \
                            Ontologies

#from ontogen.ontogen import log
#log.setLevel('DEBUG')

base_uri = 'http://purl.org/net/a4doc'


#
# classes
#


# v0.2

# TODO: limit occurrences to 1 towards
class AnnotationItem(Class):
    """abstract annotation item, use only its subclasses"""


#@AnnotationItem.isSuperclassOf
class Hint(AnnotationItem):
    """hint-like annotation item

    The content serves to express the hint itself in a text form.
    There are various types defined to optionally carry extra semantics.
    """


#
# properties
#

# v0.1

class hint(Property):
    """plain note-like annotation

    Standalone side notes for the item.
    """
    range = "http://www.w3.org/2001/XMLSchema#string"


class dangerHint(hint):
    """annotates something dangerous

    Explaining what is dangerous about touching this item (when used,
    even if with an empty string, marks the current item as
    dangerous/experimental).
    """


class deprecationHint(hint):
    """annotates something deprecated

    Details about item deprecation (when used, even if with an empty
    string, marks the current item as deprecated).
    """


class discretionHint(hint):
    """annotates something one should treat with care

    Explaining why extra caution about touching this item (when used, even
    if with an empty string, marks the current item as requiring discretion).
    """


# v0.2

@AnnotationItem.inRangeOf
class annotation(Property):
    """connects annotation item(s) to the annotated entity"""


@AnnotationItem.inDomainOf
class Type(Property):
    """defines type of the hint

    Expressed as a short symbolic string; recognized values include
    'danger', 'deprecation', and 'discretion'
    """


@AnnotationItem.inDomainOf
class content(Property):
    """the actual content implicitly attached to the annotation item"""
    range = "http://www.w3.org/2000/01/rdf-schema#Literal"  # like RDF Schema
    #range = "http://www.w3.org/1999/02/22-rdf-syntax-ns#PlainLiteral"
    #range = "http://www.w3.org/1999/02/22-rdf-syntax-ns#XMLLiteral"


#
# examples
#


class ex_relaxng_0_1(Example):
    """An example snippet within RELAX NG schema.

<optional>
  <attribute name="rrp_token_expired_timeout" a:defaultValue="47"
    a4doc:danger-hint="It is not recommended to override this value without guidance from the corosync community.">
    <a:documentation>
      This specifies the time in milliseconds to increment the problem
      counter for the redundant ring protocol after not having received a
      token from all rings for a particular processor.

      This value will automatically be calculated from the *token* timeout
      and *problem_count_threshold* but may be overridden.
    </a:documentation>
    <data type="unsignedInt"/>
  </attribute>
</optional>
    """
    pfx = 'a4doc:'


class ex_relaxng_0_2(Example):
    """An example snippet within RELAX NG schema.

<optional>
  <attribute name="rrp_token_expired_timeout" a:defaultValue="47">
    <a:documentation>
      This specifies the time in milliseconds to increment the problem
      counter for the redundant ring protocol after not having received
      a token from all rings for a particular processor.

      This value will automatically be calculated from the *token*
      timeout and *problem_count_threshold* but may be overridden.
    </a:documentation>
    <a4doc:annotation>
      <a4doc:hint type="danger">
        It is not recommended to override this value without guidance from
        the corosync community.
      </a4doc:hint>
    </a4doc:annotation>
    <data type="unsignedInt"/>
  </attribute>
</optional>
    """
    pfx = 'a4doc:'


#
# ontology + versions
#


ontologies = Ontologies()


class a4doc(OntologyWithFigure):
    #creator = XML('''\
    #    <dc:foo xmlns:dc="http://purl.org/dc/elements/1.1/"
    #    >Jan Pokorný</dc:foo>''')
    pass


@ontologies.include
class a4doc_0_1(a4doc):
    version = '0.1'
    issued = '2013-02-19'
    modified = '2013-03-20'
    properties = [
        dangerHint,
        deprecationHint,
        discretionHint,
        hint,
    ]
    examples = [
        ex_relaxng_0_1,
    ]


# different strategy than v0.1, hence no inheritance
@ontologies.include
@a4doc_0_1.supersededBy
class a4doc_0_2(a4doc):
    version = '0.2'
    issued = '2013-03-20'
    modified = '2013-03-21'
    classes = [
        AnnotationItem,
        Hint,
    ]
    properties = [
        annotation,
        content,
        Type,
    ]
    examples = [
        ex_relaxng_0_2,
    ]


if __name__ == '__main__':
    ontologies.generate_latest(**(len(argv) > 1 and {'outfile': argv[1]} or {}))
